import os

from flask import Flask
from flask_bootstrap import Bootstrap5
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_jwt_extended import JWTManager
from flask_cors import CORS

# db = SQLAlchemy()
# loginManager=LoginManager()
# jwt=JWTManager()
cors=CORS()

def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=os.path.join(app.instance_path, 'flaskr.sqlite'),
    )
    app.config['SECRET_KEY'] = 'buenastardes'
    # app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://wei:wei@localhost:3306/wei'
    # app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    # app.config["JWT_SECRET_KEY"] = "uldaricko"
    # db.init_app(app)
    # loginManager.init_app(app)
    # jwt.init_app(app)
    cors.init_app(app)
    # loginManager.login_view = "films.login"
    # bootstrap = Bootstrap5(app)
    
    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    from .metrics import metrics
    app.register_blueprint(metrics)
    
    # from .authentication import authentication
    # app.register_blueprint(authentication)
    
    

    # from .recommender import recommender
    # app.register_blueprint(recommender,url_prefix='/')

    # from .analytics import analytics
    # app.register_blueprint(analytics,url_prefix='/')

    return app
