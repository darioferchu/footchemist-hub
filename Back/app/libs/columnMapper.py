class ColumnMapper:
    def __init__(self):
        self.mapping = {
            '90s_x': 'Matches',
            'Total_Cmp%': 'Pass completion %',
            'Short_Cmp%': 'Short Pass Completion %',
            'Medium_Cmp%': 'Medium Pass Completion %',
            'Long_Cmp%': 'Long Pass Completion %',
            'Ast': 'Assists',
            'xAG': 'xAssists',
            'Expected_xA': 'Expected_xA',
            'Expected_A-xAG': 'Expected_A-xAG',
            'KP': 'Key Passes',
            '1/3': '1/3',
            'PPA': 'PPA',
            'CrsPA': 'CrsPA',
            'PrgP': 'PrgP',
            'Standard_SoT%': 'Shot on Target Percentage',
            'Standard_Sh/90': 'Shots per 90 Minutes',
            'Standard_SoT/90': 'Shots on Target per 90 Minutes',
            'Standard_G/Sh': 'Goals per Shot',
            'Standard_G/SoT': 'Goals per Shot on Target',
            'Expected_xG':'xG',  
            'Expected_npxG':'non penalty xG',
            'Expected_npxG/Sh': 'Expected Non-penalty xG per Shot',
            'Expected_np:G-xG': 'Expected Non-penalty Goals minus Expected Goals',
            'Touches_Def 3rd': 'Touches in Defensive Third',
            'Touches_Mid 3rd': 'Touches in Middle Third',
            'Touches_Att 3rd': 'Touches in Attacking Third',
            'Touches_Att Pen': 'Touches in Attacking Penalty Area',
            'Take-Ons_Succ%': 'Successful Take-Ons Percentage',
            'Take-Ons_Tkld%': 'Take-Ons Tackled Percentage',
            'Carries_PrgC': 'Progressive Carries',
            'Carries_1/3': 'Carries into Final Third',
            'Carries_CPA': 'Carries into Penalty Area',
            'Carries_Dis': 'Dispossessed during Carries',
            'Receiving_PrgR': 'Progressive Receptions',
            "('Tackles', 'Def 3rd')_defense": 'Tackles in Defensive Third',
            "('Tackles', 'Mid 3rd')_defense": 'Tackles in Middle Third',
            "('Tackles', 'Att 3rd')_defense": 'Tackles in Attacking Third',
            "('Clr', '')_defense": 'Clearances',
            "('Err', '')_defense": 'Errors Leading to Shot'
        }
        
        self.passing_columns_original=['90s_x','Total_Cmp%','Short_Cmp%','Medium_Cmp%','Long_Cmp%','Ast','xAG', 'Expected_xA', 'Expected_A-xAG',
                   'KP', '1/3', 'PPA', 'CrsPA', 'PrgP']

        #Shooting data columns
        self.shooting_columns_original=['Standard_SoT%','Standard_Sh/90','Standard_SoT/90','Standard_G/Sh','Standard_G/SoT','Expected_xG','Expected_npxG','Expected_npxG/Sh','Expected_np:G-xG']
    
        #Possession data columns
        self.possession_columns_original=['Touches_Def 3rd', 'Touches_Mid 3rd',
                   'Touches_Att 3rd','Touches_Att Pen','Take-Ons_Succ%',
                   'Take-Ons_Tkld%','Carries_PrgC', 'Carries_1/3','Carries_CPA', 'Carries_Dis','Receiving_PrgR']

        #Defense data columns
        self.defense_columns_original=["('Tackles', 'Def 3rd')_defense",
                    "('Tackles', 'Mid 3rd')_defense","('Tackles', 'Att 3rd')_defense",
                    "('Clr', '')_defense","('Err', '')_defense"]

        self.defensive_columns = ["Tackles in Defensive Third", "Tackles in Middle Third", "Tackles in Attacking Third", "Clearances", "Errors Leading to Shot"]
        self.possession_columns = ["Touches in Defensive Third", "Touches in Middle Third", "Touches in Attacking Third", "Touches in Attacking Penalty Area", "Successful Take-Ons Percentage", "Take-Ons Tackled Percentage", "Progressive Carries", "Carries into Final Third", "Carries into Penalty Area", "Dispossessed during Carries", "Progressive Receptions"]
        self.shooting_columns = ["Shot on Target Percentage", "Shots per 90 Minutes", "Shots on Target per 90 Minutes", "Goals per Shot", "Goals per Shot on Target","xG","non penalty xG","Expected Non-penalty xG per Shot", "Expected Non-penalty Goals minus Expected Goals"]
        self.passing_columns = ["Pass completion %", "Short Pass Completion %", "Medium Pass Completion %", "Long Pass Completion %", "Assists", "xAssists", "Expected_xA", "Expected_A-xAG", "Key Passes", "1/3", "PPA", "CrsPA", "PrgP"]
    
    def get_comprehensive_name(self, column_name):
        return self.mapping.get(column_name, column_name)

    def substitute_column_names(self, dataframe):
        # Create a copy of the DataFrame to avoid modifying the original DataFrame
        substituted_df = dataframe.copy()
        
        # Substitute column names using the mapping dictionary
        substituted_df.columns = [self.mapping.get(col, col) for col in dataframe.columns]
        
        return substituted_df

    def filter_defensive_columns(self, dataframe):
        return dataframe[self.defensive_columns]

    def filter_possession_columns(self, dataframe):
        return dataframe[self.possession_columns]

    def filter_shooting_columns(self, dataframe):
        return dataframe[self.shooting_columns]

    def filter_passing_columns(self, dataframe):
        return dataframe[self.passing_columns]

    def get_metrics_columns_original(self,dataframe):
        all_columns=self.passing_columns_original+self.shooting_columns_original+self.possession_columns_original+self.defense_columns_original
        return dataframe[all_columns]        

    def normalize_metrics_by_90(self,dataframe):
        
        #Passing
        dataframe['Ast']=dataframe['Ast']/dataframe['90s_x']
        dataframe['xAG']=dataframe['xAG']/dataframe['90s_x']
        dataframe['Expected_xA']=dataframe['Expected_xA']/dataframe['90s_x']
        dataframe['KP']=dataframe['KP']/dataframe['90s_x']
        dataframe['1/3']=dataframe['1/3']/dataframe['90s_x']
        dataframe['PPA']=dataframe['PPA']/dataframe['90s_x']
        dataframe['CrsPA']=dataframe['CrsPA']/dataframe['90s_x']
        dataframe['PrgP']=dataframe['PrgP']/dataframe['90s_x']
        
        #Shooting
        dataframe['Expected_xG']=dataframe['Expected_xG']/dataframe['90s_x']
        dataframe['Expected_npxG']=dataframe['Expected_npxG']/dataframe['90s_x']
        
        #Possession
        dataframe['Touches_Def 3rd']=dataframe['Touches_Def 3rd']/dataframe['90s_x']
        dataframe['Touches_Mid 3rd']=dataframe['Touches_Mid 3rd']/dataframe['90s_x']
        dataframe['Touches_Att 3rd']=dataframe['Touches_Att 3rd']/dataframe['90s_x']
        dataframe['Touches_Att Pen']=dataframe['Touches_Att Pen']/dataframe['90s_x']
        dataframe['Carries_PrgC'] = dataframe['Carries_PrgC'] / dataframe['90s_x']
        dataframe['Carries_1/3'] = dataframe['Carries_1/3'] / dataframe['90s_x']
        dataframe['Carries_CPA'] = dataframe['Carries_CPA'] / dataframe['90s_x']
        dataframe['Carries_Dis'] = dataframe['Carries_Dis'] / dataframe['90s_x']
        dataframe['Receiving_PrgR'] = dataframe['Receiving_PrgR'] / dataframe['90s_x']
        
        #Defense
        dataframe[ "('Tackles', 'Def 3rd')_defense"] = dataframe[ "('Tackles', 'Def 3rd')_defense"] / dataframe['90s_x']
        dataframe[ "('Tackles', 'Mid 3rd')_defense"] = dataframe[ "('Tackles', 'Mid 3rd')_defense"] / dataframe['90s_x']
        dataframe[ "('Tackles', 'Att 3rd')_defense"] = dataframe[ "('Tackles', 'Att 3rd')_defense"] / dataframe['90s_x']
        dataframe[ "('Clr', '')_defense"] = dataframe[ "('Clr', '')_defense"] / dataframe['90s_x']
        dataframe[ "('Err', '')_defense"] = dataframe[ "('Err', '')_defense"] / dataframe['90s_x']
        return dataframe

    def dataframe_to_csv(self,dataframe,path):
        dataframe.to_csv(path, index=False)        