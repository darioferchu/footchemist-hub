import soccerdata as sd
import pandas as pd

class FbrefAdapter:
    
    def get_fbref_data(self,league,season):
        fbref_data = sd.FBref(leagues=league, seasons=season)
        passing_stats = fbref_data.read_team_season_stats(stat_type='passing')
        shooting_stats = fbref_data.read_team_season_stats(stat_type='shooting')
        possession_stats = fbref_data.read_team_season_stats(stat_type='possession')
        defense_stats = fbref_data.read_team_season_stats(stat_type='defense')

        defense_stats.columns = [str(col) + '_defense' for col in defense_stats.columns]
        return passing_stats,shooting_stats,possession_stats,defense_stats
        
    '''
        Merge dataframes into one single dataframe
        for gathering all data, and change its names
        because it was originally a multilevel structure 
    '''
    def merge_fbref_data(self,passing_stats,shooting_stats,possession_stats,defense_stats):
        merged_stats = pd.merge(passing_stats, shooting_stats, on='team').merge(possession_stats, on='team')
        merged_stats.columns = [f'{col[0]}_{col[1]}' if col[1] else col[0] for col in merged_stats.columns]
        merged_stats = pd.merge(merged_stats,defense_stats, on='team')
        return merged_stats