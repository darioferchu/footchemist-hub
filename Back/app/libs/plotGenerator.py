import matplotlib.pyplot as plt
import numpy as np
'''
    Plot generator, for creating different types of plots
'''
class PlotGenerator:
    '''
        Plot radar char from given dataframe
    '''
    @staticmethod
    def plot_radar_chart(dataframe, title, image_path, labels=None, figsizex=20, figsizey=20):
        plt.figure(figsize=(figsizex, figsizey))

        # Extract relevant categories from the DataFrame
        categories = list(dataframe.columns)

        # Number of categories
        num_categories = len(categories)

        # Compute angle for each category
        angles = np.linspace(0, 2 * np.pi, num_categories, endpoint=False).tolist()  # Exclude the endpoint

        # Create the radar chart
        fig, ax = plt.subplots(figsize=(6, 6), subplot_kw=dict(polar=True))

        for i, (_, row) in enumerate(dataframe.iterrows()):
            set_values = row.values.tolist()  # Include all values in the row
            ax.fill(angles, set_values, alpha=0.25)
            ax_label = f'Set {i+1}'
            if labels is not None and labels[i] is not None:
                ax_label = labels[i]
            ax.plot(angles + [angles[0]], set_values + [set_values[0]], linewidth=2, label=ax_label)  # Add the first point again to close the loop

        ax.set_yticklabels([])  # Hide radial labels

        # Set the labels for each category
        ax.set_xticks(angles)
        ax.set_xticklabels(categories)

        # Add legend
        ax.legend(loc='upper right',bbox_to_anchor=(1.14, 1.14))
        plt.title(title)
        plt.savefig(image_path)
        plt.close()

    '''
        Plot bar graph from a dataframe containing two rows, the ones to compare        
    '''
    @staticmethod
    def plot_bar_plot(dataframe,image_path,xlabel,ylabel,title,labels=None,figsizex=20,figsizey=20):
        if labels is None:
            i=1
            for r in dataframe.rows:
                labels.append(str(i))
                i=i+1
        
        #Plot
        plt.figure(figsize=(figsizex, figsizey))
        print(dataframe.columns)
        for row in range(len(dataframe)):
            plt.bar(dataframe.columns, dataframe.iloc[row], label=labels[row],alpha=0.7)
        
        # Adding labels and title
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.title(title)
        plt.xticks(rotation=90)

        # Adding legend
        plt.legend()

        for col, (val1, val2) in enumerate(zip(dataframe.iloc[0], dataframe.iloc[1])):
            diff_percentage = ((val2 - val1) / val1) * 100
            plt.text(col, max(val1, val2), f'{diff_percentage:.2f}%', ha='center', va='bottom')
        
        # Displaying the plot
        plt.tight_layout()
        plt.savefig(image_path)
        plt.close()