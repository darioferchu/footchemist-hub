from flask import make_response, render_template, redirect, url_for,request
from flask import jsonify
from . import metrics

from ..libs import PlotGenerator,ColumnMapper,FbrefAdapter

# Metrics REST controller


# Test endpoint
@metrics.route('/metrics/test',methods=['GET'])
def getMetrics():
    return jsonify("Contenido"),200


# Get metrics from team, current and n-past years
@metrics.route('/metrics/team',methods=['GET'])
def getTeamMetrics():
    return jsonify("Contenido"),200

# Get metrics from team, current and n-past years
@metrics.route('/metrics/clustering',methods=['GET'])
def getTeamsClustering():
    return jsonify("Contenido"),200        

