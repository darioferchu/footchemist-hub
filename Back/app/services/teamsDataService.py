import pandas as pd
from ..libs import PlotGenerator,ColumnMapper,FbrefAdapter

def get_team_years_data(team,years):
    
    fbref_adapter=FbrefAdapter()
    passing_stats_2022,shooting_stats_2022,possession_stats_2022,defense_stats_2022=fbref_adapter.get_fbref_data('ESP-La Liga',2022)
    passing_stats_2023,shooting_stats_2023,possession_stats_2023,defense_stats_2023=fbref_adapter.get_fbref_data('ESP-La Liga',2023)

    
    merged_stats_2022=fbref_adapter.merge_fbref_data(passing_stats_2022,shooting_stats_2022,possession_stats_2022,defense_stats_2022)
    merged_stats_2023=fbref_adapter.merge_fbref_data(passing_stats_2023,shooting_stats_2023,possession_stats_2023,defense_stats_2023)
    
    barcelona_2022=merged_stats_2022.loc[[team]]
    barcelona_2023=merged_stats_2023.loc[[team]]
    
    barcelona_combined = pd.concat([barcelona_2022, barcelona_2023])

    numerical_columns = barcelona_combined.select_dtypes(include='number').columns
    barcelona_data = barcelona_combined[numerical_columns]

    column_mapper = ColumnMapper()
    
    barcelona_data=column_mapper.get_metrics_columns_original(barcelona_data)

    
    barcelona_data=column_mapper.normalize_metrics_by_90(barcelona_data)
    
    
    # column_mapper.dataframe_to_csv(barcelona_data,os.path.join(figures_dir, 'final_dataframe.csv'))
    

    labels=['Barcelona 2022','Barcelona 2023']
    
    plot_gen = PlotGenerator()
    
    barcelona_formatted_dataframe=column_mapper.substitute_column_names(barcelona_data)

    
    barcelona_formatted_passing=column_mapper.filter_passing_columns(barcelona_formatted_dataframe)
    barcelona_formatted_shooting=column_mapper.filter_shooting_columns(barcelona_formatted_dataframe)
    barcelona_formatted_possession=column_mapper.filter_possession_columns(barcelona_formatted_dataframe)
    barcelona_formatted_defense=column_mapper.filter_defensive_columns(barcelona_formatted_dataframe)
    
    